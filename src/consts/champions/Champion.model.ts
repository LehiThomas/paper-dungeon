import { Monster, MonsterAttack } from '../monsters/Monster.model';

export interface Stance {
  name: string;
  description: string;
  attackName: string;
  damage: number;
  block: number;
  heal: number;
  damageBuff: number;
  blockBuff: number;
  healBuff: number;
}

export enum TokenTypes {
  Burn = 'BURN',
  Chill = 'CHILL',
  Decay = 'DECAY',
  Poison = 'POISON',
}

export interface Card {
  name: string;
  description: string;
  aggro: number;
  damage: number;
  block: number;
  heal: number;
  damageBuff: number;
  blockBuff: number;
  healBuff: number;
  blockAbility: boolean;
  singleTarget: boolean;
  tokens?: {
    amount: number;
    type: TokenTypes;
  };
}

export interface Stats {
  damage: number;
  block: number;
  heal: number;
  damageBuff: number;
  blockBuff: number;
  healBuff: number;
}

export interface Tokens {
  burn: number;
  decay: number;
  chill: number;
  poison: number;
}

export interface Intention {
  action: Card;
  target: Champion;
}

export class Champion {
  public id: string = '';
  public name: string = '';
  public health: number = 10;
  public level: number = 0;
  public diceAggro: number = 0;
  public baseAggro: number = 0;
  public isTargetable: boolean = true;
  public stances: Card[] = [];
  public selectedStance: Card | null = null;
  public attacks: Card[] = [];
  public stats: Stats = {
    damage: 0,
    block: 0,
    heal: 0,
    damageBuff: 0,
    blockBuff: 0,
    healBuff: 0,
  };
  public intention: Intention | null = null;
  public tokens: Tokens = {
    burn: 0,
    decay: 0,
    chill: 0,
    poison: 0,
  };

  constructor(
    name: string,
    baseHealth: number,
    aggro: number,
    stances: Card[],
    attacks: Card[]
  ) {
    this.name = name;
    this.health = baseHealth;
    this.baseAggro = aggro;
    this.id = Date.now().toString();

    this.setStances(stances);
    this.setAttacks(attacks);
  }

  setStances(championStances: Card[]): void {
    this.stances = championStances;
  }

  setStance(stance: Card): void {
    this.selectedStance = stance;
  }

  setAttacks(championAttacks: Card[]): void {
    this.attacks = championAttacks;
  }

  setIntention(intention: Card, target: any): void {
    this.intention = {
      action: intention,
      target: target,
    };
  }

  drawCards(): Card[] {
    let card1: number;
    let card2: number;
    card1 = Math.floor(Math.random() * this.attacks.length);
    card2 = Math.floor(Math.random() * this.attacks.length);
    while (card1 === card2) {
      card2 = Math.floor(Math.random() * this.attacks.length);
    }
    return [this.attacks[card1], this.attacks[card2]];
  }

  computeAggro(newAggro: number, type: string): void {
    if (type === 'base') {
      this.baseAggro += newAggro;
    } else if (type === 'dice') {
      this.diceAggro += newAggro;
    }
  }

  getTotalAggro(): number {
    return this.baseAggro + this.diceAggro;
  }

  public resolveIntention(roundIntentions: any): void {
    if (this.intention) {
      // intention type = attack
      roundIntentions.damage += this.intention?.action.damage;
      if (this.intention?.action.tokens) {
        const type = this.intention?.action.tokens.type;
        roundIntentions.tokens[type] += this.intention?.action.tokens.amount;
      }
    }

    if (this.intention) {
      this.stats.damage += this.intention?.action.damage;
      this.stats.block += this.intention?.action.block;
      this.stats.heal += this.intention?.action.heal;
      this.stats.damageBuff += this.intention?.action.damageBuff;
      this.stats.blockBuff += this.intention?.action.blockBuff;
      this.stats.healBuff += this.intention?.action.healBuff;
    }
    if (this.selectedStance) {
      this.stats.damage += this.selectedStance?.damage;
      this.stats.block += this.selectedStance?.block;
      this.stats.heal += this.selectedStance?.heal;
      this.stats.damageBuff += this.selectedStance?.damageBuff;
      this.stats.blockBuff += this.selectedStance?.blockBuff;
      this.stats.healBuff += this.selectedStance?.healBuff;
    }
  }

  public takeDamage(damage: number): void {
    this.health = this.health - damage;
  }

  public reset() {
    this.stats = {
      damage: 0,
      block: 0,
      heal: 0,
      damageBuff: 0,
      blockBuff: 0,
      healBuff: 0,
    };
    this.intention = null;
    this.selectedStance = null;
    this.diceAggro = 0;
  }
}
