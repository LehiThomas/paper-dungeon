import DRAGON from './dragon.json';
import GOBLIN from './goblin.json';
import LICH from './lich.json';
import MINDFLARE from './mindflare.json';
import SKELETON from './skeleton.json';

export const dragon = DRAGON;
export const goblin = GOBLIN;
export const lich = LICH;
export const mindflare = MINDFLARE;
export const skeleton = SKELETON;

export const MONSTERS: any = {
  dragon,
  goblin,
  lich,
  skeleton,
  mindflare,
};
