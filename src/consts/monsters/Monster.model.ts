import { Tokens, TokenTypes } from '../champions/Champion.model';
import { rollDice } from '../../util/helpers';

export interface MonsterAttack {
  name: string;
  description: string;
  damage: number;
  attackReduction: number | undefined;
  singleTarget: boolean;
  heal?: number;
  tokens?: {
    amount: number;
    type: TokenTypes;
  };
}

export interface Intention {
  action: MonsterAttack;
  target: any;
}

export class Monster {
  public id: string = '';
  public name: string = '';
  public health: number = 0;
  public attacks: MonsterAttack[] = [];
  public attackReduction: number = 0;
  public stats: any = {
    damage: 0,
    block: 0,
    heal: 0,
  };
  public intention: Intention | null = null;
  public tokens: Tokens = {
    burn: 0,
    decay: 0,
    chill: 0,
    poison: 0,
  };

  constructor(monsterData: any, round: number) {
    this.name = monsterData.name;
    this.health = monsterData.baseHealth * round;
    this.id = Date.now().toString();

    this.setAttacks(monsterData.attacks);
  }

  setAttacks(attacks: MonsterAttack[]): void {
    this.attacks = attacks;
  }

  setIntention(intention: MonsterAttack, target: any): void {
    this.intention = {
      action: intention,
      target: target,
    };
  }

  determineAttack(target: any): void {
    let diceRoll = rollDice(6);
    let attack = this.attacks.find((attackObj: any) =>
      attackObj.diceNumber.includes(diceRoll)
    );

    if (attack) {
      const victim = attack.singleTarget ? target : 'all';
      this.setIntention(attack, victim);
    }
  }

  public resolveMonsterBuffs(): void {
    if (this.intention && this.intention.action.attackReduction) {
      this.attackReduction = this.intention?.action.attackReduction;
    }
  }

  public takeDamage(damage: number): void {
    this.health = this.health - damage;
  }

  public reset() {
    this.intention = null;
    this.attackReduction = 0;
  }
}
