import React, { useState, useEffect } from 'react';
import { Card } from './consts/champions/Champion.model';
import { Brains } from './util/Brains';
import './App.css';

import { rollDice } from './util/helpers';

function App() {
  const [champ, setChamp] = useState('monk');
  const [game, setGame] = useState<Brains | null>(null);
  const [phase, setPhase] = useState('');
  const [playerId, setPlayerId] = useState('');

  useEffect(() => {
    const startGame: () => void = () => {
      const newGame = new Brains();
      setGame(newGame);
    };
    startGame();
  }, []);

  function createPlayer(): void {
    if (game) {
      const id = game.addPlayer(champ);
      setPlayerId(id);
      setPhase('draw');
    }
  }

  const selectCard: (card: Card) => void = (card: Card) => {
    game?.players[playerId].setIntention(card, game.monster);
    game?.players[playerId].computeAggro(card.aggro, 'base');
    setPhase('intent');
    console.log('~~~~~~', game);
  };

  const drawCards: () => JSX.Element | JSX.Element[] = () => {
    const moves = game?.players[playerId].drawCards();
    if (moves) {
      return moves.map((attack: Card) => {
        return (
          <button className='player-card' onClick={() => selectCard(attack)}>
            <h4>{attack.name}</h4>
            <p>{attack.description}</p>
            <p>Aggro: {attack.aggro}</p>
          </button>
        );
      });
    } else {
      return <></>;
    }
  };

  const setStance: (stanceName: string) => void = (stanceName) => {
    let stanceObj = game?.players[playerId].stances.find(
      (stance: Card) => stance.name === stanceName
    );
    if (stanceObj) {
      game?.players[playerId].setStance(stanceObj);
    }
  };

  function aggroRoll(): void {
    let diceRoll = rollDice(20);
    game?.players[playerId].computeAggro(diceRoll, 'dice');
    setPhase('monster-roll');
  }

  function monsterRoll(): void {
    game?.determineMonsterIntention();

    resolveRound();
    setPhase('resolve');
  }

  function resolveRound(): void {
    game?.resolvePlayerIntentions();
    game?.resolveRound();
    console.log('End of Round Results: ', game?.roundOutcome);
  }

  function nextRound(): void {
    setPhase('draw');
    game?.startNewRound();
  }

  return (
    <div className='App'>
      {game !== null &&
        (playerId === '' ? (
          <header className='App-section'>
            Select A Champion
            <select
              value={champ}
              onChange={(event) => setChamp(event.target.value)}
            >
              <option value='monk'>Monk</option>
              <option value='mage'>Mage</option>
            </select>
            <button onClick={createPlayer}>Select Player</button>
          </header>
        ) : (
          <>
            <div className='monster-card'>
              <h5>Round {game?.round}</h5>
              <h3>{game?.monster?.name}</h3>
              <h4>Health: {game?.monster?.health}</h4>
              {phase === 'monster-roll' && (
                <section>
                  <button onClick={monsterRoll}>
                    Roll for {game?.monster?.name}'s Attack
                  </button>
                </section>
              )}
              {game?.monster?.intention !== null && (
                <div>
                  {game?.monster?.intention.action.name}: Damage{' '}
                  {game?.monster?.intention &&
                    game?.monster?.intention.action.damage * game.round}
                </div>
              )}
            </div>
            {playerId !== '' && (
              <div className='player-section'>
                <header className='App-section'>
                  {game?.players[playerId].name}
                </header>
                <h4>
                  Health: {game?.players[playerId].health} | Aggro :
                  {game?.players[playerId].baseAggro +
                    game?.players[playerId].diceAggro}
                </h4>
                {game?.players[playerId].selectedStance !== null && (
                  <section>
                    <div>
                      Stance: {game?.players[playerId].selectedStance.name}
                    </div>
                    <div className='player-card'>
                      <h4>{game?.players[playerId].intention.action.name}</h4>
                      <p className='card-description'>
                        {game?.players[playerId].intention.action.description}
                      </p>
                    </div>
                  </section>
                )}
                {phase === 'draw' && (
                  <>
                    <select onChange={(event) => setStance(event.target.value)}>
                      <option disabled selected value=''>
                        Select a Stance
                      </option>
                      {game?.players[playerId].stances.map((stance: any) => {
                        return (
                          <option value={stance.name}>
                            {stance.name}: {stance.description}
                          </option>
                        );
                      })}
                    </select>
                    <section className='card-container'>{drawCards()}</section>
                  </>
                )}
                {phase === 'intent' && (
                  <section>
                    <button onClick={aggroRoll}>Roll for Aggro</button>
                  </section>
                )}
              </div>
            )}
            {phase === 'resolve' && (
              <>
                <div>
                  <h4>Player Buffs:</h4>
                  <ul>
                    <li>
                      Damage Buff: {game?.players[playerId].stats.damageBuff}
                    </li>
                    <li>
                      Block Buff: {game?.players[playerId].stats.blockBuff}
                    </li>
                    <li>Heal Buff: {game?.players[playerId].stats.healBuff}</li>
                  </ul>
                </div>
                <div>
                  <h4>Player Damage:</h4>
                  <ul>
                    <li>Damage: {game?.players[playerId].stats.damage}</li>
                    <li>Block: {game?.players[playerId].stats.block}</li>
                    <li>Heal: {game?.players[playerId].stats.heal}</li>
                  </ul>
                </div>
                <div>
                  <h4>Monster Buffs:</h4>
                  <ul>
                    <li>Attack Reduction: {game?.attackReduction}</li>
                  </ul>
                </div>
                <h3>{game.roundOutcome}</h3>
                <button onClick={() => nextRound()}>Next Round</button>
              </>
            )}
          </>
        ))}
    </div>
  );
}

export default App;
