export const rollDice: (x: number) => number = (x) => {
  return Math.floor(Math.random() * x) + 1;
};
