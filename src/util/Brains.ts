import { MONSTERS } from '../consts/monsters';
import { Monk } from '../consts/champions/Monk';
import { Mage } from '../consts/champions/Mage';
import { Monster } from '../consts/monsters/Monster.model';
import { Tokens } from '../consts/champions/Champion.model';

const monstersArray: string[] = [
  'dragon',
  'goblin',
  'lich',
  'mindflare',
  'skeleton',
];

export class Brains {
  public round: number;
  public monster: Monster | null = null;
  public players: any = {};
  public attackReduction: number = 0;
  public roundOutcome: string = '';
  public intentions: any = {
    damage: 0,
    tokens: {
      burn: 0,
      decay: 0,
      chill: 0,
      poison: 0,
    },
  };

  constructor() {
    this.round = 1;
    this.createMonster();
  }

  public createMonster: () => void = () => {
    const mon = monstersArray[Math.floor(Math.random() * monstersArray.length)];
    this.monster = new Monster(MONSTERS[mon], this.round);
  };

  public addPlayer(player: string): string {
    let newPlayer = null;
    switch (player) {
      case 'monk':
        newPlayer = new Monk();
        break;
      case 'mage':
        newPlayer = new Mage();
        break;
      default:
        break;
    }

    if (newPlayer) {
      this.players[newPlayer.id] = newPlayer;
      return newPlayer.id;
    }
    return 'error';
  }

  public resolvePlayerIntentions(): void {
    for (const player in this.players) {
      this.players[player].resolveIntention(this.intentions);
    }
  }

  determineMonsterIntention(): void {
    let target;
    for (const player in this.players) {
      if (target === undefined) {
        target = this.players[player];
      } else {
        if (this.players[player].getTotalAggro() > target.getTotalAggro()) {
          target = this.players[player];
        }
      }
    }
    this.monster?.determineAttack(target);
  }

  public resolveRound(): void {
    let teamDamage = this.intentions.damage;
    console.log('Total Player Damage = ');
    if (this.monster && this.monster.attackReduction > 0) {
      teamDamage -= (teamDamage * this.attackReduction) / 100;
    }

    if (this.monster) {
      let target = this.monster.intention?.target;
      this.monster.health = this.monster.health - teamDamage;
      this.roundOutcome = `${this.monster.name} was attacked by team for ${teamDamage} damage.`;
      console.log('Monsters Health = ', this.monster.health);

      if (this.monster.health < 1) {
        this.monster.health = 0;
        this.roundOutcome = `${this.roundOutcome} You defeated the ${this.monster.name}.`;
      } else {
        if (this.monster.intention && this.monster.intention.action) {
          let monsterDamage: number =
            this.monster.intention.action?.damage * this.round;
          if (target === 'all') {
            // Attack everybody
            for (const player in this.players) {
              monsterDamage =
                monsterDamage -
                this.players[player].stats.block -
                this.players[player].stats.heal;
              if (monsterDamage > 0) {
                this.players[player].takeDamage(monsterDamage);
                this.roundOutcome = `${this.roundOutcome} ${this.players[player].name} was hit for ${monsterDamage}.`;
              }
              if (this.monster.intention.action?.tokens) {
                const { type, amount } = this.monster.intention.action?.tokens;
                this.players[player].tokens[type] = amount;
              }
              if (this.monster.intention.action?.heal) {
                this.monster.health += this.monster.intention.action?.heal;
              }
            }
          } else if (target) {
            monsterDamage =
              monsterDamage - target.stats.block - target.stats.heal;
            if (monsterDamage > 0) {
              target.takeDamage(monsterDamage);
              this.roundOutcome = `${this.roundOutcome} ${target.name} was hit for ${monsterDamage}.`;
            }
          }
        }
      }
    }
  }

  startNewRound() {
    this.round += 1;
    if (this.monster && this.monster.health < 1) {
      this.createMonster();
    } else {
      this.monster?.reset();
    }
    for (const player in this.players) {
      this.players[player].reset();
    }
  }
}
